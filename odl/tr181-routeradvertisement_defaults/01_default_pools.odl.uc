{% let hasAnyUpstream = BDfn.hasAnyUpstream() %}
%populate {
    object RouterAdvertisement.InterfaceSetting {
{% for (let Bridge in BD.Bridges) : %}
        instance add(0, '{{lc(Bridge)}}',
                     Enable ={% if (hasAnyUpstream) : %} true{% else %} false{% endif %},
                     Interface = "${ip_intf_{{lc(Bridge)}}}",
                     AdvOtherConfigFlag = true,
                     AdvPreferredRouterFlag = "Medium",
                     MaxRtrAdvInterval = 600,
                     MinRtrAdvInterval = 200,
                     AdvDefaultLifetime = 1800,
                     RDNSSMode = "GUA",
                     AdvRDNSSLifetime = 1800,
                     AdvDNSSLLifetime = 1800,
                     AdvLinkMTU = 1500,
                     Controller="mod-ra-radvd")
        {}
{% endfor; %}
    }
}
