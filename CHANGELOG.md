# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.18.14 - 2024-12-03(11:01:37 +0000)

### Other

- - [routeradvertisement] A and L flags must be set by default

## Release v0.18.13 - 2024-11-21(12:40:29 +0000)

### Other

- [tr181-routeradvertisement] Implement support for manual prefixes

## Release v0.18.12 - 2024-11-05(16:55:01 +0000)

### Other

- [tr181-RouterAdvertisement] ULA64 prefix not showing up in the prefixes

## Release v0.18.11 - 2024-10-24(12:37:08 +0000)

### Other

- 

## Release v0.18.10 - 2024-09-11(09:02:22 +0000)

### Other

- [tr181-routeradvertisement] User parameter AdvManagedFlag must be upgrade persistent

## Release v0.18.9 - 2024-09-10(08:29:16 +0000)

### Other

- - "[Network Settings][IPv6]: IPv6 Configuration control settings not preserved after firmware upgrade"

## Release v0.18.8 - 2024-09-10(08:13:23 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds

## Release v0.18.7 - 2024-09-10(06:49:42 +0000)

### Other

- [LAN RA] DUT transmit a multicast ICMPv6 RA to the LAN before completing the SARR cycle

## Release v0.18.6 - 2024-07-23(07:57:52 +0000)

### Fixes

- Better shutdown script

## Release v0.18.5 - 2024-07-11(09:53:09 +0000)

### Other

- [tr181-RouterAdvertisement] [ip-manager] Differentiate a static and a dynamic on mode on the wan interface.

## Release v0.18.4 - 2024-07-08(07:02:48 +0000)

## Release v0.18.3 - 2024-07-02(15:07:52 +0000)

### Other

- - TR-181: Device.RouterAdvertisement data model issues 19.03.2024

## Release v0.18.2 - 2024-06-17(10:08:11 +0000)

### Other

- create default configuration based on bridges in networklayout.json
- - amx plugin should not run as root user
- For AP, both RouterAdvertisement instances must be disabled

## Release v0.18.1 - 2024-04-10(07:11:39 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.18.0 - 2024-04-09(10:02:36 +0000)

### New

- [IPv6][Server]Upon IPv6 deactivation, "Valid Lifetime" and "Preferred Lifetime" values in RA not equal to 0

## Release v0.17.0 - 2024-03-26(10:43:21 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.16.1 - 2024-03-18(12:47:12 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.16.0 - 2024-03-14(15:17:47 +0000)

### New

- [TR181-RA][TR181-IP][IPv6][Server]No RA is sent to LAN clients when deactivating IPv6

## Release v0.15.0 - 2024-01-25(14:27:00 +0000)

### New

- TR181-RouterAdvertisement missing parameters

## Release v0.14.7 - 2023-12-12(11:33:50 +0000)

### Fixes

- [Repeater Config][IPv6] Implement a proper IPv6 config of the repeater

## Release v0.14.6 - 2023-11-30(09:01:11 +0000)

### Other

- Set `lla` controllers by default

## Release v0.14.5 - 2023-10-13(13:45:54 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.14.4 - 2023-10-10(11:34:25 +0000)

### Fixes

- [TR181-RouterAdvertisement] Advertised prefixes are not correctly deprecated when the DHCPv6 delegated prefix changes

## Release v0.14.3 - 2023-09-21(11:25:21 +0000)

### Fixes

- [Fut][Random]Youtube and facebook are not accessible with a browser (firefox) - no RA on LAN

## Release v0.14.2 - 2023-07-14(09:29:18 +0000)

### Other

- [tr181-logical][LAN mib] The Lan mib must expose the domainSearchList parameter

## Release v0.14.1 - 2023-07-03(12:43:01 +0000)

### Fixes

- Init script has no shutdown function

## Release v0.14.0 - 2023-06-08(09:32:54 +0000)

### New

- [CDROUTER][IPv6] The prefix lifetime decrementation seems occuring with a delay

## Release v0.13.0 - 2023-06-01(10:54:47 +0000)

### New

- [CDROUTER][IPv6] The Box is still advertising itself as router at reception on WAN of RA with router lifetime 0

## Release v0.12.8 - 2023-05-11(09:18:54 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v0.12.7 - 2023-05-09(13:16:05 +0000)

### Other

- [TR181-RouterAdvertisement][IPv6] Managed address configuration flag (M bit) is '1'; should be '0'

## Release v0.12.6 - 2023-05-09(12:24:14 +0000)

### Fixes

- [CDROUTER][IPv6] When WAN is down, the box is still anouncing Router lifetime as non null in its RA

## Release v0.12.5 - 2023-04-13(12:29:45 +0000)

### Fixes

- [CDROUTER][IPv6] When WAN is down, the box is still anouncing Router lifetime as non null in its RA

## Release v0.12.4 - 2023-03-15(10:43:26 +0000)

### Fixes

- Still invalid prefix in the Prefixes parameter

## Release v0.12.3 - 2023-03-09(12:15:36 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.12.2 - 2023-03-02(08:58:51 +0000)

### Fixes

- Box sends 2 ICMPv6 RA when a RS is received on LAN

### Other

- Add missing runtime dependency on rpcd

## Release v0.12.1 - 2023-02-07(08:13:30 +0000)

### Fixes

- [TR181][Routeradvertisment] wait for IP

## Release v0.12.0 - 2023-01-31(08:19:09 +0000)

### New

- [tr181][routeradvertisment] does not update prefixes

## Release v0.11.0 - 2023-01-20(13:47:49 +0000)

### New

- [tr181][Routeradvertisement] move netmodel query to modules

## Release v0.10.0 - 2023-01-05(12:05:15 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v0.9.0 - 2023-01-05(11:41:57 +0000)

### New

- [prpl][tr181-routeradvertisment] implement radvd module

## Release v0.8.1 - 2022-12-09(09:30:52 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.8.0 - 2022-10-14(12:09:49 +0000)

### New

- Update the README

## Release v0.7.2 - 2022-06-23(17:18:29 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v0.7.1 - 2022-04-25(11:05:35 +0000)

### Changes

- [tr181][RouterAdvertisement] add unit tests

## Release v0.7.0 - 2022-04-21(12:49:20 +0000)

### New

- [tr181][RouterAdvertisement] startup should override the uci variables

## Release v0.6.0 - 2022-04-21(11:28:19 +0000)

### New

- [TR181][RouterAdvertisement] The RAplugin must listen on asynchronous (interface) events

## Release v0.5.1 - 2022-03-24(10:34:47 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.5.0 - 2022-03-03(12:48:43 +0000)

### New

- [CONFIG] add lcm interface to config

## Release v0.4.1 - 2022-02-25(11:42:22 +0000)

### Other

- Enable core dumps by default

## Release v0.4.0 - 2021-12-21(19:33:04 +0000)

### New

- subscribe to ipv6prefixes

## Release v0.3.2 - 2021-12-09(15:12:05 +0000)

### Other

- Skip check of ci jobs

## Release v0.3.1 - 2021-12-09(13:18:36 +0000)

### Fixes

- fix loading defaults

## Release v0.3.0 - 2021-12-01(11:56:08 +0000)

### New

- mod-ra-uci: initial commit

## Release v0.2.1 - 2021-11-25(11:07:16 +0000)

### Other

- [CI] Add .gitlab-ci.yml

## Release v0.2.0 - 2021-11-23(17:16:20 +0000)

### New

- separation of concerns: datamodel manager

## Release v0.1.0 - 2021-11-16(08:24:20 +0000)

### New

- initial files

