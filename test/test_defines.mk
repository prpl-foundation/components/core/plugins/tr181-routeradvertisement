MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
SRVSRCDIR = $(realpath ../../src/server)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include_priv ../mocks)
MOCK_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		  -Wno-unused-but-set-variable

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxp -lamxd -lamxb -lamxo -lamxm \
		   -lsahtrace -ldl -lpthread -lnetmodel
