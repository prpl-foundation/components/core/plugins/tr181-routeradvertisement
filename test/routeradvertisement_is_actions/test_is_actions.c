/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "ra_priv.h"
#include "ra_dm_mngr.h"
#include "ra_object_info.h"
#include "test_is_actions.h"
#include "../mocks/mock.h"

#define RA_ODL_DEF "../../odl/tr181-routeradvertisement_definition.odl"

static amxd_dm_t dm;
static amxo_parser_t parser;

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

int __wrap_amxb_get(amxb_bus_ctx_t* const bus_ctx,
                    const char* object,
                    int32_t depth,
                    amxc_var_t* ret,
                    int timeout);

int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so) {
    printf("mocking amxm_so_open: %s %s\n", shared_object_name, path_to_so);
    return 0;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    printf("func_name: %s\n", func_name);
    function_called();
    return 0;
}

static void handle_events(void) {
    printf("Handling events");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int __wrap_amxb_get(UNUSED amxb_bus_ctx_t* const bus_ctx,
                    UNUSED const char* object,
                    UNUSED int32_t depth,
                    amxc_var_t* ret,
                    UNUSED int timeout) {
    amxc_var_t obj_ht;
    amxc_var_t values;
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_init(&values);
    amxc_var_init(&obj_ht);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&obj_ht, AMXC_VAR_ID_HTABLE);
    if(strstr(object, "IPv6Prefix") == NULL) {
        amxc_var_add_key(cstring_t, &values, "Alias", "lan");
        amxc_var_add_key(amxc_htable_t, &obj_ht, "Device.IP.Interface.3", amxc_var_constcast(amxc_htable_t, &values));
    } else {
        /* generating dummy list of ipv6prefixes */
        amxc_var_add_key(cstring_t, &values, "dummy", "dummyval");
        amxc_var_add_key(amxc_htable_t, &obj_ht, "Device.IP.Interface.3.IPv6Prefix.1.", amxc_var_constcast(amxc_htable_t, &values));
        amxc_var_add_key(amxc_htable_t, &obj_ht, "Device.IP.Interface.3.IPv6Prefix.2.", amxc_var_constcast(amxc_htable_t, &values));
    }
    amxc_var_add(amxc_htable_t, ret, amxc_var_constcast(amxc_htable_t, &obj_ht));
    amxc_var_clean(&values);
    amxc_var_clean(&obj_ht);
    return AMXB_STATUS_OK;
}

static void enable_parent(bool enable) {
    amxd_trans_t trans;
    amxd_object_t* ra_interface_obj = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    ra_interface_obj = amxd_dm_findf(&dm, "RouterAdvertisement.");
    assert_non_null(ra_interface_obj);

    amxd_trans_select_object(&trans, ra_interface_obj);

    amxd_trans_set_value(bool, &trans, "Enable", enable);

    status = amxd_trans_apply(&trans, &dm);
    assert_true(status == amxd_status_ok);

    amxd_trans_clean(&trans);
}

static void change_intf_name(cstring_t alias, cstring_t new_intf_name) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "RouterAdvertisement.InterfaceSetting.%s.", alias);
    amxd_trans_set_value(cstring_t, &trans, "Interface", new_intf_name);

    status = amxd_trans_apply(&trans, &dm);
    assert_true(status == amxd_status_ok);

    amxd_trans_clean(&trans);
}

static int set_manual_prefixes(cstring_t alias, cstring_t prefixes, bool remove) {
    int rv = -1;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxc_string_t tmp_string;
    amxc_var_t tmp_var;
    amxd_object_t* ra_obj = NULL;

    const char* manual_prefixes = NULL;

    amxc_string_init(&tmp_string, 0);
    amxc_var_init(&tmp_var);
    amxd_trans_init(&trans);

    if(str_empty(alias) || str_empty(prefixes)) {
        goto exit;
    }

    ra_obj = amxd_dm_findf(&dm, "RouterAdvertisement.InterfaceSetting.[Alias == '%s']", alias);
    assert_int_equal(amxd_object_get_params(ra_obj, &tmp_var, amxd_dm_access_protected), 0);
    manual_prefixes = GET_CHAR(&tmp_var, "ManualPrefixes");

    amxc_string_setf(&tmp_string, "%s", manual_prefixes);
    if(remove) {
        amxc_string_replace(&tmp_string, prefixes, "", 1);
    } else {
        if(amxc_string_search(&tmp_string, prefixes, 0) == -1) {
            amxc_string_prependf(&tmp_string, "%s,", prefixes);
        }
    }

    amxd_trans_select_pathf(&trans, "RouterAdvertisement.InterfaceSetting.%s.", alias);
    amxd_trans_set_value(cstring_t, &trans, "ManualPrefixes", amxc_string_get(&tmp_string, 0));

    status = amxd_trans_apply(&trans, &dm);
    assert_true(status == amxd_status_ok);

    rv = 0;
exit:
    amxd_trans_clean(&trans);
    amxc_var_clean(&tmp_var);
    amxc_string_clean(&tmp_string);
    return rv;
}

static void set_prefixes(cstring_t alias, cstring_t prefixes, cstring_t converted_prefixes) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_pathf(&trans, "RouterAdvertisement.InterfaceSetting.%s.", alias);
    amxd_trans_set_value(cstring_t, &trans, "Prefixes", prefixes);
    amxd_trans_set_value(cstring_t, &trans, "ConvertedPrefixes", converted_prefixes);
    amxd_trans_set_value(cstring_t, &trans, "RelativeValidLifetimes", "3600");
    amxd_trans_set_value(cstring_t, &trans, "RelativePreferredLifetimes", "2500");
    amxd_trans_set_value(cstring_t, &trans, "Autonomous", "true");
    amxd_trans_set_value(cstring_t, &trans, "OnLink", "true");

    status = amxd_trans_apply(&trans, &dm);
    assert_true(status == amxd_status_ok);

    amxd_trans_clean(&trans);
}

static void enable_intf(cstring_t alias, bool enable) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "RouterAdvertisement.InterfaceSetting.%s.", alias);
    amxd_trans_set_value(bool, &trans, "Enable", enable);

    status = amxd_trans_apply(&trans, &dm);
    assert_true(status == amxd_status_ok);

    amxd_trans_clean(&trans);
}

static void add_interface(const cstring_t alias, const cstring_t intf_name) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "RouterAdvertisement.InterfaceSetting.");
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, "Alias", alias);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", intf_name);
    amxd_trans_set_value(cstring_t, &trans, "AdvPreferredRouterFlag", "Low");
    amxd_trans_set_value(bool, &trans, "AdvMobileAgentFlag", true);

    status = amxd_trans_apply(&trans, &dm);
    assert_true(status == amxd_status_ok);

    amxd_trans_clean(&trans);
    handle_events();
}

static void remove_interface(const char* alias) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "RouterAdvertisement.InterfaceSetting.");
    amxd_trans_del_inst(&trans, 0, alias);

    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();

    amxd_trans_clean(&trans);
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "ra_delete_is",
                                            AMXO_FUNC(_ra_delete_is)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ra_global_enable_changed",
                                            AMXO_FUNC(_ra_global_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ra_interfacesetting_added",
                                            AMXO_FUNC(_ra_interfacesetting_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ra_interfacesetting_enable_changed",
                                            AMXO_FUNC(_ra_interfacesetting_enable_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ra_interfacesetting_interface_changed",
                                            AMXO_FUNC(_ra_interfacesetting_interface_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ra_interfacesetting_param_changed",
                                            AMXO_FUNC(_ra_interfacesetting_param_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "ra_manual_prefix_changed",
                                            AMXO_FUNC(_ra_manual_prefix_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "is_unique",
                                            AMXO_FUNC(_is_unique)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "check_empty_or_in",
                                            AMXO_FUNC(_check_empty_or_in)), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, RA_ODL_DEF, root_obj), 0);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_add_del_is_from_dm(UNUSED void** state) {
    assert_int_equal(_ra_main(0, &dm, &parser), 0);

    //check if the external module is called for adding an instance
    expect_function_call(__wrap_amxm_execute_function);
    add_interface("someinterface", "Device.IP.Interface.3.");
    handle_events();

    //remove the interface and check if the external modules are updated
    expect_function_call(__wrap_amxm_execute_function);
    remove_interface("someinterface");
    handle_events();

    assert_int_equal(_ra_main(1, &dm, &parser), 0);
}

void test_enable_disable_instance(UNUSED void** state) {
    amxd_object_t* ra_interface_obj = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    //START
    assert_int_equal(_ra_main(0, &dm, &parser), 0);

    expect_function_call(__wrap_amxm_execute_function);
    add_interface("test", "Device.IP.Interface.2.");
    handle_events();

    //Check if it is Enabled correctly
    ra_interface_obj = amxd_dm_findf(&dm, "RouterAdvertisement.InterfaceSetting.[Alias == 'test']");
    assert_non_null(ra_interface_obj);
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    //disable instance
    enable_intf("test", false);
    expect_function_call(__wrap_amxm_execute_function);
    handle_events();
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    //enable instance
    enable_intf("test", true);
    expect_function_call(__wrap_amxm_execute_function);
    handle_events();
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    expect_function_call(__wrap_amxm_execute_function);
    remove_interface("test");
    handle_events();

    //STOP
    assert_int_equal(_ra_main(1, &dm, &parser), 0);

    amxc_var_clean(&params);
}

void test_global_enable_disable_instance(UNUSED void** state) {
    amxd_object_t* ra_interface_obj = NULL;
    amxc_var_t params;

    amxc_var_init(&params);

    assert_int_equal(_ra_main(0, &dm, &parser), 0);

    //Add interface
    expect_function_call(__wrap_amxm_execute_function);
    add_interface("testGlobalEnable", "Device.IP.Interface.3.");
    handle_events();

    //Check enabled
    ra_interface_obj = amxd_dm_findf(&dm, "RouterAdvertisement.InterfaceSetting.[Alias == 'testGlobalEnable']");
    assert_non_null(ra_interface_obj);
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    //disable instance
    enable_parent(false);
    expect_function_call(__wrap_amxm_execute_function);
    handle_events();
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    //enable instance
    enable_parent(true);
    expect_function_call(__wrap_amxm_execute_function);
    handle_events();
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    expect_function_call(__wrap_amxm_execute_function);
    remove_interface("testGlobalEnable");
    handle_events();

    assert_int_equal(_ra_main(1, &dm, &parser), 0);

    amxc_var_clean(&params);
}

void test_intf_changed(UNUSED void** state) {
    amxd_object_t* ra_interface_obj = NULL;
    amxc_var_t params;
    amxc_var_init(&params);

    assert_int_equal(_ra_main(0, &dm, &parser), 0);

    //Add interface
    expect_function_call(__wrap_amxm_execute_function);
    add_interface("testChangeIntf", "Device.IP.Interface.4.");
    handle_events();

    //Check enabled
    ra_interface_obj = amxd_dm_findf(&dm, "RouterAdvertisement.InterfaceSetting.[Alias == 'testChangeIntf']");
    assert_non_null(ra_interface_obj);
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    //Change intf name
    expect_function_call(__wrap_amxm_execute_function); //first mod-remove should be called
    expect_function_call(__wrap_amxm_execute_function); //After this mod-update should be called
    change_intf_name("testChangeIntf", "incorrectName");
    handle_events();
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    assert_int_equal(_ra_main(1, &dm, &parser), 0);

    amxc_var_clean(&params);
}

void test_deprecated_prefixes(UNUSED void** state) {
    amxd_object_t* ra_interface_obj = NULL;
    amxc_var_t params;
    ra_info_t* ra_info = NULL;
    amxc_var_init(&params);

    assert_int_equal(_ra_main(0, &dm, &parser), 0);

    //Add interface
    expect_function_call(__wrap_amxm_execute_function);
    add_interface("testDeprecatedPrefixes", "Device.IP.Interface.3.");
    handle_events();

    //Check enabled
    ra_interface_obj = amxd_dm_findf(&dm, "RouterAdvertisement.InterfaceSetting.[Alias == 'testDeprecatedPrefixes']");
    assert_non_null(ra_interface_obj);
    ra_info = (ra_info_t*) ra_interface_obj->priv;
    assert_non_null(ra_info);
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    expect_function_call(__wrap_amxm_execute_function);
    set_prefixes("testDeprecatedPrefixes", "Device.IP.Interface.3.IPv6Prefix.1.", "fc00:0:0:1::/64");
    handle_events();

    expect_function_call(__wrap_amxm_execute_function);
    set_prefixes("testDeprecatedPrefixes", "Device.IP.Interface.3.IPv6Prefix.2.", "2a02:1802:94:33f0::/64");
    handle_events();

    assert_string_equal(GETP_CHAR(ra_info->send_mod_data, "DeprecatedPrefixes.0"), "fc00:0:0:1::/64");

    expect_function_call(__wrap_amxm_execute_function);
    set_prefixes("testDeprecatedPrefixes", "Device.IP.Interface.3.IPv6Prefix.2.", "fc00:0:0:1::/64");
    handle_events();

    assert_string_equal(GETP_CHAR(ra_info->send_mod_data, "DeprecatedPrefixes.0"), "2a02:1802:94:33f0::/64");

    expect_function_call(__wrap_amxm_execute_function);
    remove_interface("testDeprecatedPrefixes");
    handle_events();

    assert_int_equal(_ra_main(1, &dm, &parser), 0);

    amxc_var_clean(&params);
}

void test_ipv6_disable(UNUSED void** state) {
    amxd_object_t* ra_interface_obj = NULL;
    amxc_var_t params;
    amxc_var_t event_args;
    ra_info_t* ra_info = NULL;
    amxc_var_init(&params);
    amxc_var_init(&event_args);

    assert_int_equal(_ra_main(0, &dm, &parser), 0);

    //Add interface
    expect_function_call(__wrap_amxm_execute_function);
    add_interface("lan", "Device.IP.Interface.3.");
    handle_events();

    //Check enabled
    ra_interface_obj = amxd_dm_findf(&dm, "RouterAdvertisement.InterfaceSetting.[Alias == 'lan']");
    assert_non_null(ra_interface_obj);
    ra_info = (ra_info_t*) ra_interface_obj->priv;
    assert_non_null(ra_info);
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    expect_function_call(__wrap_amxm_execute_function);
    set_prefixes("lan", "Device.IP.Interface.3.IPv6Prefix.2.", "2a02:1802:94:33f0::/64");
    handle_events();

    amxc_var_set_type(&event_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &event_args, "Interface", "IP.Interface.3.");

    expect_function_call(__wrap_amxm_execute_function);
    ra_ipv6_going_down(NULL, &event_args, NULL);
    handle_events();

    assert_int_equal(GET_UINT32(ra_info->send_mod_data, "AdvDefaultLifetime"), 0);
    assert_int_equal(GET_UINT32(ra_info->send_mod_data, "AdvRDNSSLifetime"), 0);
    assert_int_equal(GET_UINT32(ra_info->send_mod_data, "AdvDNSSLifetime"), 0);
    assert_string_equal(GET_CHAR(ra_info->send_mod_data, "RelativePreferredLifetimes"), "0");
    assert_string_equal(GET_CHAR(ra_info->send_mod_data, "RelativeValidLifetimes"), "0");
    assert_string_equal(GET_CHAR(ra_info->send_mod_data, "Autonomous"), "true");
    assert_string_equal(GET_CHAR(ra_info->send_mod_data, "OnLink"), "true");
    assert_true(ra_info->ipv6_disable_rcvd);

    amxc_var_clean(&event_args);
    amxc_var_init(&event_args);
    amxc_var_set_type(&event_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &event_args, "Interface", "IP.Interface.123.");

    // don't expect call to module here, because IP interface is invalid
    ra_ipv6_going_down(NULL, &event_args, NULL);
    handle_events();

    amxc_var_clean(&event_args);
    amxc_var_init(&event_args);
    amxc_var_set_type(&event_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &event_args, "Interface", "");

    // expect mod-shutdown call for each module
    expect_function_call(__wrap_amxm_execute_function);
    expect_function_call(__wrap_amxm_execute_function);
    ra_ipv6_going_down(NULL, &event_args, NULL);
    handle_events();

    assert_int_equal(_ra_main(1, &dm, &parser), 0);

    amxc_var_clean(&params);
    amxc_var_clean(&event_args);
}

void test_ipv6_static_prefix(UNUSED void** state) {
    amxd_object_t* ra_interface_obj = NULL;
    amxd_object_t* root_obj = NULL;
    amxc_var_t params;
    amxc_var_t event_args;
    amxc_var_t* static_prefix;
    ra_info_t* ra_info = NULL;
    global_ra_info_t* global_ra_info = NULL;
    amxc_var_init(&params);
    amxc_var_init(&event_args);

    assert_int_equal(_ra_main(0, &dm, &parser), 0);

    //Add interface
    expect_function_call(__wrap_amxm_execute_function);
    add_interface("prefix_test", "Device.IP.Interface.4.");
    handle_events();

    //Check enabled
    ra_interface_obj = amxd_dm_findf(&dm, "RouterAdvertisement.InterfaceSetting.[Alias == 'prefix_test']");
    assert_non_null(ra_interface_obj);
    ra_info = (ra_info_t*) ra_interface_obj->priv;
    assert_non_null(ra_info);

    root_obj = amxd_dm_findf(&dm, "RouterAdvertisement.");
    assert_non_null(root_obj);
    global_ra_info = (global_ra_info_t*) root_obj->priv;
    assert_non_null(global_ra_info);

    expect_function_call(__wrap_amxm_execute_function);
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    expect_function_call(__wrap_amxm_execute_function);
    set_prefixes("prefix_test", "Device.IP.Interface.3.IPv6Prefix.2.", "2a02:1802:94:33f0::/64");
    handle_events();

    //Prepare the event_args to be filled with a static prefix instance
    amxc_var_set_type(&event_args, AMXC_VAR_ID_LIST);
    static_prefix = amxc_var_add_new(&event_args);
    amxc_var_set_type(static_prefix, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, static_prefix, "Index", 4);
    amxc_var_add_key(cstring_t, static_prefix, "Origin", "Static");

    expect_function_call(__wrap_amxm_execute_function);
    trigger_getIPv6Prefixes_callback(&event_args);
    handle_events();

    //Check if the router lifetimes are indeed still kept active
    assert_int_equal(global_ra_info->nm_query_wan_router_lft, 0);

    expect_function_call(__wrap_amxm_execute_function);
    ra_ipv6_going_down(NULL, &event_args, NULL);
    handle_events();

    amxc_var_clean(&event_args);
    amxc_var_init(&event_args);
    amxc_var_set_type(&event_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &event_args, "Interface", "");

    expect_function_call(__wrap_amxm_execute_function);
    ra_ipv6_going_down(NULL, &event_args, NULL);
    handle_events();

    assert_int_equal(_ra_main(1, &dm, &parser), 0);

    amxc_var_clean(&params);
    amxc_var_clean(&event_args);
}

void test_skip_interface(UNUSED void** state) {
    amxd_object_t* ra_interface_obj = NULL;
    amxc_var_t params;
    ra_info_t* ra_info = NULL;
    amxc_var_init(&params);

    assert_int_equal(_ra_main(0, &dm, &parser), 0);

    //Add interface
    expect_function_call(__wrap_amxm_execute_function);
    add_interface("testSkipInterface", "Device.IP.Interface.7.");
    handle_events();

    //Check enabled
    ra_interface_obj = amxd_dm_findf(&dm, "RouterAdvertisement.InterfaceSetting.[Alias == 'testSkipInterface']");
    assert_non_null(ra_interface_obj);
    ra_info = (ra_info_t*) ra_interface_obj->priv;
    assert_non_null(ra_info);
    assert_int_equal(amxd_object_get_params(ra_interface_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    assert_false(GET_BOOL(ra_info->send_mod_data, "Enable"));

    expect_function_call(__wrap_amxm_execute_function);
    set_prefixes("testSkipInterface", "Device.IP.Interface.7.IPv6Prefix.1.", "2a02:1802:94:33f0::/64");
    handle_events();

    assert_true(GET_BOOL(ra_info->send_mod_data, "Enable"));

    assert_int_equal(_ra_main(1, &dm, &parser), 0);

    amxc_var_clean(&params);
}

void test_populate_manualprefixes_parameter(UNUSED void** state) {
    amxd_object_t* ra_obj = NULL;
    amxc_var_t params;
    amxc_var_t event_args;
    amxc_var_t* static_prefix = NULL;

    amxc_var_init(&params);
    amxc_var_init(&event_args);

    assert_int_equal(_ra_main(0, &dm, &parser), 0);

    //Add interface
    expect_function_call(__wrap_amxm_execute_function);
    add_interface("MyTest", "Device.IP.Interface.8.");
    handle_events();

    ra_obj = amxd_dm_findf(&dm, "RouterAdvertisement.InterfaceSetting.[Alias == 'MyTest']");
    assert_int_equal(amxd_object_get_params(ra_obj, &params, amxd_dm_access_protected), 0);

    //Adding the manual prefix and looking for the reaction on the datamodel
    expect_function_call(__wrap_amxm_execute_function);
    set_manual_prefixes("MyTest", "IP.Interface.8.IPv6Prefix.2.", 0);
    set_manual_prefixes("MyTest", "IP.Interface.8.IPv6Prefix.5.", 0);
    handle_events();

    //Adding netmodel prefixes
    amxc_var_set_type(&event_args, AMXC_VAR_ID_LIST);
    static_prefix = amxc_var_add_new(&event_args);
    amxc_var_set_type(static_prefix, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, static_prefix, "Index", 2);
    amxc_var_add_key(cstring_t, static_prefix, "Origin", "Static");
    amxc_var_add_key(cstring_t, static_prefix, "StaticType", "Static");
    amxc_var_add_key(cstring_t, static_prefix, "Prefix", "2a02:1210:127b:2e80::/64");

    static_prefix = amxc_var_add_new(&event_args);
    amxc_var_set_type(static_prefix, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, static_prefix, "Index", 3);
    amxc_var_add_key(cstring_t, static_prefix, "Origin", "Static");
    amxc_var_add_key(cstring_t, static_prefix, "StaticType", "Static");
    amxc_var_add_key(cstring_t, static_prefix, "Prefix", "2a02:1210:127b:2e80::/64");

    static_prefix = amxc_var_add_new(&event_args);
    amxc_var_set_type(static_prefix, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, static_prefix, "Index", 4);
    amxc_var_add_key(cstring_t, static_prefix, "Origin", "Child");
    amxc_var_add_key(cstring_t, static_prefix, "StaticType", "Inapplicable");
    amxc_var_add_key(cstring_t, static_prefix, "Prefix", "2a02:1210:127b:2e80::/64");

    static_prefix = amxc_var_add_new(&event_args);
    amxc_var_set_type(static_prefix, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, static_prefix, "Index", 5);
    amxc_var_add_key(cstring_t, static_prefix, "Origin", "Static");
    amxc_var_add_key(cstring_t, static_prefix, "StaticType", "Static");
    amxc_var_add_key(cstring_t, static_prefix, "Prefix", "2a02:1210:127b:2e80::/64");

    expect_function_call(__wrap_amxm_execute_function);
    trigger_manual_getIPv6Prefixes_callback(&event_args);
    handle_events();

    assert_int_equal(amxd_object_get_params(ra_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Prefixes"), "Device.IP.Interface.8.IPv6Prefix.2.,Device.IP.Interface.8.IPv6Prefix.4.,Device.IP.Interface.8.IPv6Prefix.5.");

    //Removing the manual prefix and looking at the reaction on the datamodel
    expect_function_call(__wrap_amxm_execute_function);
    set_manual_prefixes("MyTest", "IP.Interface.8.IPv6Prefix.2.", 1);
    handle_events();

    expect_function_call(__wrap_amxm_execute_function);
    trigger_manual_getIPv6Prefixes_callback(&event_args);
    handle_events();

    assert_int_equal(amxd_object_get_params(ra_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Prefixes"), "Device.IP.Interface.8.IPv6Prefix.4.,Device.IP.Interface.8.IPv6Prefix.5.");

    assert_int_equal(_ra_main(1, &dm, &parser), 0);

    amxc_var_clean(&params);
    amxc_var_clean(&event_args);
}
