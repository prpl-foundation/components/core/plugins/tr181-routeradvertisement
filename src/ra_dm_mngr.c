/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <debug/sahtrace.h>
#include "ra_dm_mngr.h"

static void dm_mngr_load_controllers(void) {
    SAH_TRACEZ_IN(ME);
    amxo_parser_t* parser = ra_get_parser();
    amxd_dm_t* dm = ra_get_dm();
    amxd_object_t* ra = amxd_dm_findf(dm, "RouterAdvertisement.");
    const amxc_var_t* controllers = amxd_object_get_param_value(ra, "SupportedControllers");
    amxc_var_t lcontrollers;
    amxm_shared_object_t* so = NULL;

    amxc_var_init(&lcontrollers);
    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        amxc_string_t mod_path;
        amxc_string_init(&mod_path, 0);
        const char* name = GET_CHAR(controller, NULL);

        amxc_string_setf(&mod_path, "${mod-path}/%s.so", name);
        amxc_string_resolve_var(&mod_path, &parser->config);
        if(amxm_so_open(&so, name, amxc_string_get(&mod_path, 0)) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to load %s", amxc_string_get(&mod_path, 0));
        }
        amxc_string_clean(&mod_path);
    }
    amxc_var_clean(&lcontrollers);
    SAH_TRACEZ_OUT(ME);
}

static int ra_subscribe_signals(void) {
    int ret = -1;
    amxb_bus_ctx_t* ip_ctx = amxb_be_who_has("IP.");

    when_null_trace(ip_ctx, exit, ERROR, "No bus found for IP");

    ret = amxb_subscribe(ip_ctx, "IP.", "notification == 'ip:ipv6_going_down'", ra_ipv6_going_down, NULL);
    when_failed_trace(ret, exit, ERROR, "Failed to subscribe to IP events: %d", ret);
exit:
    return ret;
}

void ra_dm_mngr_init(void) {
    SAH_TRACEZ_IN(ME);
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    when_null(so, exit);

    when_failed_trace(amxm_module_register(&mod, so, MOD_DM_MNGR), exit, ERROR, "Failed to register modules");
    dm_mngr_load_controllers();
    when_failed_trace(ra_subscribe_signals(), exit, ERROR, "Failed to subscribe to signals");
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

int ra_dm_mngr_cleanup(void) {
    SAH_TRACEZ_IN(ME);
    amxm_close_all();
    SAH_TRACEZ_OUT(ME);
    return 0;
}
