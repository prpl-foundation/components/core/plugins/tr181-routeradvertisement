/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "ra_update_mod.h"
#include "ra_object_info.h"
#include "ra_netmodel.h"

#define DNS_MODE_STATIC "Static"
#define DNS_MODE_GUA "GUA"
#define DNS_MODE_LLA "LLA"
#define DNS_MODE_LLA_GUA "LLA_GUA"

static bool ra_is_object_changed(amxc_var_t* parameters) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* to_delete = NULL;
    bool changed = 0;

    when_null_trace(parameters, exit, ERROR, "Bad input variable");

    to_delete = amxc_var_take_key(parameters, "Status");
    amxc_var_delete(&to_delete);

    changed = (amxc_var_get_first(parameters) != NULL);
exit:
    SAH_TRACEZ_OUT(ME);
    return changed;
}

static void set_deprecated_prefixes(amxd_object_t* object, amxc_var_t* deprecated_prefixes) {
    ra_info_t* ra_info = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null_trace(deprecated_prefixes, exit, ERROR, "Deprecated prefixes list is NULL");

    ra_info = (ra_info_t*) object->priv;
    when_null_trace(ra_info, exit, ERROR, "Object private info is NULL");

    when_null_trace(amxc_var_get_first(deprecated_prefixes), exit, INFO, "Deprecated prefixes list is empty");

    if(ra_info->deprecated_prefixes == NULL) {
        amxc_var_new(&(ra_info->deprecated_prefixes));
        amxc_var_move(ra_info->deprecated_prefixes, deprecated_prefixes);
    } else if(amxc_var_is_null(ra_info->deprecated_prefixes)) {
        amxc_var_move(ra_info->deprecated_prefixes, deprecated_prefixes);
    } else {
        amxc_var_for_each(var, deprecated_prefixes) {
            amxc_var_add(cstring_t, ra_info->deprecated_prefixes, GET_CHAR(var, NULL));
        }
    }

exit:
    return;
}

static void depr_pref_timer_cb(amxp_timer_t* timer, void* priv) {
    amxd_object_t* instance = (amxd_object_t*) priv;
    ra_info_t* ra_info = NULL;
    when_null(instance, exit);
    ra_info = instance->priv;
    when_null(ra_info, exit);

    /*
     * By now, we should have advertised the deprecated prefixes for long enough.
     * It should be safe to delete the accumulated list of deprecated prefixes.
     */
    if(ra_info->deprecated_prefixes != NULL) {
        amxc_var_delete(&(ra_info->deprecated_prefixes));
    }

    ra_update(instance, false, NORMAL_UPDATE);
exit:
    amxp_timer_stop(timer);
}

/*
 * This function returns a list containing the elements that are in first but not in second.
 * The result is stored in first.
 */
static void prefix_list_calc_difference(amxc_var_t* first, amxc_var_t* second) {
    amxc_string_t second_str;

    amxc_string_init(&second_str, 0);

    amxc_string_set(&second_str, GET_CHAR(second, NULL));
    amxc_var_cast(first, AMXC_VAR_ID_LIST);

    amxc_var_for_each(var, first) {
        if(amxc_string_search(&second_str, GET_CHAR(var, NULL), 0) != -1) {
            amxc_var_take_it(var);
            amxc_var_delete(&var);
        }
    }
    amxc_string_clean(&second_str);
}

static void handle_deprecated_prefixes(amxd_object_t* instance,
                                       amxc_var_t* old_prefixes,
                                       amxc_var_t* new_prefixes) {
    when_null(instance, exit);
    if((old_prefixes != NULL) && (new_prefixes != NULL)) {
        amxc_var_t new_deprecated_prefixes;
        ra_info_t* ra_info = (ra_info_t*) instance->priv;
        uint32_t min_rtr_interval = amxd_object_get_value(uint32_t, instance, "MinRtrAdvInterval", NULL);

        when_null_trace(ra_info, exit, ERROR, "Object private data is NULL");
        amxc_var_init(&new_deprecated_prefixes);

        // If a prefix was deprecated but is now again a valid prefix, remove it from the deprecated prefixes
        if(ra_info->deprecated_prefixes != NULL) {
            prefix_list_calc_difference(ra_info->deprecated_prefixes, new_prefixes);
        }

        // Determine the new deprecated prefixes
        amxc_var_copy(&new_deprecated_prefixes, old_prefixes);
        prefix_list_calc_difference(&new_deprecated_prefixes, new_prefixes);

        set_deprecated_prefixes(instance, &new_deprecated_prefixes);
        amxc_var_clean(&new_deprecated_prefixes);

        /*
         * Start a timer to give some time for the deprecated prefix to be advertised.
         * When the timer runs out, the deprecated prefixes should be deleted.
         */
        if(ra_info->depr_pref_timer == NULL) {
            amxp_timer_new(&(ra_info->depr_pref_timer), depr_pref_timer_cb, instance);
        }
        amxp_timer_start(ra_info->depr_pref_timer, min_rtr_interval * 1000);
    }
exit:
    return;
}

amxd_status_t _check_empty_or_in(amxd_object_t* object,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;

    status = amxd_action_param_check_is_in(object, param, reason,
                                           args, retval, priv);

    if(status != amxd_status_ok) {
        char* input = amxc_var_dyncast(cstring_t, args);
        if((input == NULL) || (*input == 0)) {
            status = amxd_status_ok;
        }
        free(input);
    }
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void enable_changed(amxd_object_t* instance) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(instance, exit, ERROR, "bad parameter given");
    if(is_instance_enabled(instance)) {
        ra_update_prefix_subscription(instance);
        ra_update(instance, false, NORMAL_UPDATE);
    } else {
        ra_remove(instance, true);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ra_global_enable_changed(UNUSED const char* const event_name,
                               const amxc_var_t* const event_data,
                               UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = amxd_dm_signal_get_object(ra_get_dm(), event_data);
    amxd_object_t* intf_setting_inst = NULL;
    bool enable = GETP_BOOL(event_data, "parameters.Enable.to");

    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");

    if(instance->priv == NULL) {
        when_failed_trace(global_ra_info_create(instance), exit, ERROR, "Failed to create global ra info");
    }

    if(enable) {
        ra_netmodel_subscribe_wan(instance);
    } else {
        ra_netmodel_unsubscribe_wan(instance);
    }

    amxd_object_iterate(instance, it, amxd_object_get(instance, "InterfaceSetting")) {
        intf_setting_inst = amxc_container_of(it, amxd_object_t, it);
        enable_changed(intf_setting_inst);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void rdnss_query_cb(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_trans_t trans;
    amxc_string_t rdnss_addr;
    ra_info_t* info = NULL;

    when_null_trace(priv, exit, ERROR, "No private info provided");
    info = (ra_info_t*) priv;

    amxc_string_init(&rdnss_addr, 0);
    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, info->obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxc_var_for_each(addr, data) {
        if(amxc_string_text_length(&rdnss_addr) > 0) {
            amxc_string_appendf(&rdnss_addr, ",%s", GET_CHAR(addr, "Address"));
        } else {
            amxc_string_setf(&rdnss_addr, "%s", GET_CHAR(addr, "Address"));
        }
    }
    amxd_trans_set_value(csv_string_t, &trans, "RDNSS", amxc_string_get(&rdnss_addr, 0));

    rv = amxd_trans_apply(&trans, ra_get_dm());
    when_failed_trace(rv, exit, ERROR, "Failed to apply transaction, return '%d'", rv);

exit:
    amxd_trans_clean(&trans);
    amxc_string_clean(&rdnss_addr);
    SAH_TRACEZ_OUT(ME);
}

static void rdnss_query_start(amxd_object_t* instance) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t params;
    ra_info_t* info = NULL;
    amxd_param_t* rdnss_param = NULL;
    const char* dns_mode = NULL;
    const char* flag = NULL;
    bool is_persistent = false;

    amxc_var_init(&params);

    when_null_trace(instance, exit, ERROR, "Bad input param instance");
    info = (ra_info_t*) instance->priv;

    if(info->nm_query_rdnss_address != NULL) {
        netmodel_closeQuery(info->nm_query_rdnss_address);
        info->nm_query_rdnss_address = NULL;
    }

    amxd_object_get_params(instance, &params, amxd_dm_access_protected);
    dns_mode = GET_CHAR(&params, "RDNSSMode");
    when_str_empty_trace(dns_mode, exit, ERROR, "Failed to get dns mode");
    if(strcmp(dns_mode, DNS_MODE_STATIC) == 0) {
        is_persistent = true;
    } else if(strcmp(dns_mode, DNS_MODE_GUA) == 0) {
        flag = "@gua";
    } else if(strcmp(dns_mode, DNS_MODE_LLA) == 0) {
        flag = "@lla";
    } else if(strcmp(dns_mode, DNS_MODE_LLA_GUA) == 0) {
        flag = "@gua || @lla";
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown DNS mode '%s'", dns_mode);
        goto exit;
    }

    rdnss_param = amxd_object_get_param_def(instance, "RDNSS");
    rv = amxd_param_set_attr(rdnss_param, amxd_pattr_persistent, is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the persistent flag", is_persistent ? "add" : "remove");
    rv = amxd_param_set_attr(rdnss_param, amxd_pattr_read_only, !is_persistent);
    when_failed_trace(rv, exit, ERROR, "Failed to %s the readonly flag", !is_persistent ? "add" : "remove");

    // if the RDNSSMode is static, the RDNSS mode is persistent and we do not want to start a NetModel query
    if(!is_persistent) {
        info->nm_query_rdnss_address = netmodel_openQuery_getAddrs(GET_CHAR(&params, "Interface"), ME, flag, netmodel_traverse_this, rdnss_query_cb, info);
        when_null_trace(info->nm_query_rdnss_address, exit, ERROR, "Failed to open query to get RDNSS addresses");
    }

exit:
    amxc_var_clean(&params);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ra_interfacesetting_added(UNUSED const char* const event_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = NULL;
    amxd_object_t* obj = amxd_dm_signal_get_object(ra_get_dm(), event_data);

    when_null_trace(obj, exit, ERROR, "Failed to find the added instance");
    instance = amxd_object_get_instance(obj, NULL, GET_UINT32(event_data, "index"));
    when_null_trace(instance, exit, ERROR, "Failed to find the added instance");

    if(instance->priv == NULL) {
        ra_info_create(instance);
        ra_update_prefix_subscription(instance);
        rdnss_query_start(instance);
        ra_update(instance, true, NORMAL_UPDATE);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ra_interfacesetting_enable_changed(UNUSED const char* const event_name,
                                         const amxc_var_t* const event_data,
                                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = amxd_dm_signal_get_object(ra_get_dm(), event_data);
    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");

    enable_changed(instance);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ra_interfacesetting_interface_changed(UNUSED const char* const event_name,
                                            const amxc_var_t* const event_data,
                                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = amxd_dm_signal_get_object(ra_get_dm(), event_data);
    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");

    ra_remove(instance, true);

    when_str_empty(GETP_CHAR(event_data, "parameters.Interface.to"), exit);
    ra_update_prefix_subscription(instance);
    ra_update(instance, false, NORMAL_UPDATE);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ra_rdnss_mode_changed(UNUSED const char* const event_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = amxd_dm_signal_get_object(ra_get_dm(), event_data);
    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");

    rdnss_query_start(instance);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ra_interfacesetting_param_changed(UNUSED const char* const event_name,
                                        const amxc_var_t* const event_data,
                                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* instance = amxd_dm_signal_get_object(ra_get_dm(), event_data);
    amxc_var_t* old_prefixes = NULL;
    amxc_var_t* new_prefixes = NULL;

    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");
    when_false(ra_is_object_changed(GET_ARG(event_data, "parameters")), exit);

    old_prefixes = GETP_ARG(event_data, "parameters.ConvertedPrefixes.from");
    new_prefixes = GETP_ARG(event_data, "parameters.ConvertedPrefixes.to");

    handle_deprecated_prefixes(instance, old_prefixes, new_prefixes);

    ra_update(instance, false, NORMAL_UPDATE);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _ra_delete_is(amxd_object_t* object,
                            amxd_param_t* param,
                            amxd_action_t reason,
                            const amxc_var_t* const args,
                            amxc_var_t* const retval,
                            void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    ra_info_t* info = NULL;
    when_null(object, exit);

    info = (ra_info_t*) object->priv;
    if(info != NULL) {
        ra_remove(object, false);
        ra_info_clear(object);
    }
exit:
    rv = amxd_action_object_destroy(object, param, reason, args, retval, priv);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _is_unique(amxd_object_t* object,
                         UNUSED amxd_param_t* param,
                         amxd_action_t reason,
                         const amxc_var_t* const args,
                         amxc_var_t* const retval,
                         UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;
    amxd_object_t* ra_obj = NULL;
    cstring_t current_interface_name = NULL;
    cstring_t interface_name = NULL;

    if(reason != action_param_validate) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    current_interface_name = amxc_var_dyncast(cstring_t, args);
    when_str_empty(current_interface_name, exit);

    ra_obj = amxd_object_get_parent(object);
    when_null(ra_obj, exit);
    amxd_object_iterate(instance, it, ra_obj) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        if(inst == object) {
            continue;
        }

        interface_name = amxd_object_get_value(cstring_t, inst, "Interface", NULL);
        if((interface_name != NULL) && (strcmp(interface_name, current_interface_name) == 0)) {
            status = amxd_status_invalid_name;
            free(interface_name);
            amxc_var_set(bool, retval, false);
            goto exit;
        }
        free(interface_name);
    }
    amxc_var_set(bool, retval, true);
exit:
    free(current_interface_name);
    SAH_TRACEZ_OUT(ME);
    return status;
}

void ra_ipv6_going_down(UNUSED const char* const event_name,
                        const amxc_var_t* const event_data,
                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);

    if(!str_empty(GET_CHAR(event_data, "Interface"))) {
        amxd_object_t* instance = amxd_dm_findf(ra_get_dm(),
                                                "RouterAdvertisement.InterfaceSetting.[Interface=='Device.%s' && Enable==true]",
                                                GET_CHAR(event_data, "Interface"));
        if(instance != NULL) {
            SAH_TRACEZ_INFO(ME, "IPv6 was disabled for interface %s, notify clients", GET_CHAR(event_data, "Interface"));
            ra_info_t* ra_info = instance->priv;
            when_null_trace(ra_info, exit, ERROR, "Instance private data is NULL");

            ra_info->ipv6_disable_rcvd = 1;
            ra_update(instance, false, FORCE_BACKEND);
        } else {
            SAH_TRACEZ_INFO(ME, "IPv6 was disabled for %s, but no InterfaceSetting with that interface found", GET_CHAR(event_data, "Interface"));
        }
    } else {
        SAH_TRACEZ_INFO(ME, "IPv6 disabled globally, shutdown modules");
        amxd_object_t* ra = amxd_dm_findf(ra_get_dm(), "RouterAdvertisement.");
        const amxc_var_t* controllers = amxd_object_get_param_value(ra, "SupportedControllers");
        amxc_var_t lcontrollers;

        amxc_var_init(&lcontrollers);
        amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
        amxc_var_for_each(controller, &lcontrollers) {
            amxc_var_t ret;
            amxc_var_t args;
            amxc_var_init(&args);
            amxc_var_init(&ret);
            when_failed_trace(amxm_execute_function(GET_CHAR(controller, NULL),
                                                    GET_CHAR(controller, NULL),
                                                    "mod-shutdown",
                                                    &args,
                                                    &ret), loop_end, ERROR, "mod-shutdown call failed for module %s", GET_CHAR(controller, NULL));
loop_end:
            amxc_var_clean(&ret);
            amxc_var_clean(&args);
        }
        amxc_var_clean(&lcontrollers);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _ra_manual_prefix_changed(UNUSED const char* const event_name,
                               const amxc_var_t* const event_data,
                               UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    ra_info_t* info = NULL;
    amxd_object_t* instance = amxd_dm_signal_get_object(ra_get_dm(), event_data);

    info = (ra_info_t*) instance->priv;
    when_null_trace(info, exit, ERROR, "No priv structure in object");

    //Reopen query with the right flags for manual prefixes
    ra_netmodel_open_ipv6prefix_query(info);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
