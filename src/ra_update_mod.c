/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "ra_update_mod.h"
#include "ra_object_info.h"
#include "ra_dm_mngr.h"
#include "ra_netmodel.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

typedef enum ra_status {
    status_enabled,
    status_disabled,
    status_error_misconfigured,
    status_error
} ra_status_t;

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

static void ra_set_status(amxd_object_t* obj, ra_status_t status) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    switch(status) {
    case status_enabled:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
        break;
    case status_disabled:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
        break;
    case status_error_misconfigured:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error_Misconfigured");
        break;
    case status_error:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error");
        break;
    default:
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error_Misconfigured");
        SAH_TRACEZ_ERROR(ME, "Invalid status '%d' value is given", status);
    }

    if(amxd_trans_apply(&trans, ra_get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply the transaction for setting the status");
    }
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
}

bool is_instance_enabled(amxd_object_t* instance) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* server = amxd_dm_findf(ra_get_dm(), "RouterAdvertisement.");
    bool server_enable = amxd_object_get_value(bool, server, "Enable", NULL);
    bool obj_enable = true;

    when_null_trace(instance, exit, ERROR, "bad parameter given");

    obj_enable = amxd_object_get_value(bool, instance, "Enable", NULL);
exit:
    SAH_TRACEZ_OUT(ME);
    return (server_enable && obj_enable);
}

void ra_update(amxd_object_t* instance, bool first_time, update_type_t update_type) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t data;
    ra_info_t* info = NULL;
    int result_cmp = -1;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    when_null_trace(instance, exit, ERROR, "Bad input param instance");
    info = (ra_info_t*) instance->priv;
    when_null_trace(info, exit, ERROR, "Could not find the priv parameter");
    when_null_trace(instance, exit, ERROR, "Could not find the obj parameter of info");
    when_false((is_instance_enabled(instance) || first_time), exit_ignore);

    when_failed_trace(ra_get_params(instance, &data), exit, ERROR, "Failed to retrieve parameters");
    when_failed_trace(amxc_var_compare(info->send_mod_data, &data, &result_cmp), exit, ERROR, "Failed to compare data");
    when_true((result_cmp == 0) && (update_type == NORMAL_UPDATE), exit_ignore);

    if(info->send_mod_data == NULL) {
        amxc_var_new(&info->send_mod_data);
    }
    amxc_var_copy(info->send_mod_data, &data);

    if(update_type == FORCE_BACKEND) {
        amxc_var_add_key(bool, &data, "Force", true);
    }

    status = (amxd_status_t) amxm_execute_function(GET_CHAR(info->send_mod_data, "Controller"),
                                                   GET_CHAR(info->send_mod_data, "Controller"),
                                                   "mod-update",
                                                   &data,
                                                   &ret);

    if(status != amxd_status_ok) {
        amxc_var_delete(&info->send_mod_data);
        info->send_mod_data = NULL;
        SAH_TRACEZ_ERROR(ME, "Failed to execute amxm_execute_function");
    }

exit:
    ra_set_status(instance, status == amxd_status_ok ? (is_instance_enabled(instance) ? status_enabled : status_disabled) : status_error);
exit_ignore:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
}

void ra_update_prefix_subscription(amxd_object_t* instance) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    ra_info_t* info = NULL;

    when_null_trace(instance, exit, ERROR, "Bad input param instance");
    info = (ra_info_t*) instance->priv;
    when_null_trace(info, exit, ERROR, "Could not find the priv parameter");
    when_false(is_instance_enabled(instance), exit_ignore);

    status = ra_netmodel_subscribe_prefixes(info);
    when_failed_trace(status, exit, ERROR, "Failed to start a subscription");
exit:
    ra_set_status(instance, status == amxd_status_ok ? status_enabled : status_error);
exit_ignore:
    SAH_TRACEZ_OUT(ME);
    return;
}

void ra_remove(amxd_object_t* instance, bool set_status) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    amxc_var_t data;
    amxd_status_t status = amxd_status_unknown_error;
    ra_info_t* info = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    when_null_trace(instance, exit, ERROR, "Bad input param instance");
    info = (ra_info_t*) instance->priv;
    when_null_trace(info, exit, ERROR, "Could not find the priv parameter");

    when_null(info->send_mod_data, exit_ignore);

    ra_netmodel_unsubscribe_prefixes(info);

    status = (amxd_status_t) amxm_execute_function(GET_CHAR(info->send_mod_data, "Controller"),
                                                   GET_CHAR(info->send_mod_data, "Controller"),
                                                   "mod-remove",
                                                   info->send_mod_data,
                                                   &ret);
    when_failed_trace(status, exit, ERROR, "Failed to execute amxm_execute_function");
    amxc_var_delete(&info->send_mod_data);
    info->send_mod_data = NULL;
exit:
    if(set_status) {
        ra_set_status(instance, status == amxd_status_ok ? status_disabled : status_error);
    }
exit_ignore:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void set_csv_str_zero(amxc_var_t* csv_var) {
    amxc_var_t list_var;

    when_null_trace(csv_var, exit, ERROR, "Variant is NULL");

    amxc_var_init(&list_var);
    amxc_var_convert(&list_var, csv_var, AMXC_VAR_ID_LIST);
    amxc_var_for_each(el, &list_var) {
        amxc_var_set(uint32_t, el, 0);
    }
    amxc_var_clean(csv_var);
    amxc_var_convert(csv_var, &list_var, AMXC_VAR_ID_CSV_STRING);
    amxc_var_clean(&list_var);
exit:
    return;
}

static void set_all_lifetimes_zero(amxc_var_t* params) {
    when_null_trace(params, exit, ERROR, "Params is NULL");

    set_csv_str_zero(GET_ARG(params, "RelativePreferredLifetimes"));
    set_csv_str_zero(GET_ARG(params, "RelativeValidLifetimes"));

    amxc_var_set(uint32_t, GET_ARG(params, "AdvRDNSSLifetime"), 0);
    amxc_var_set(uint32_t, GET_ARG(params, "AdvDNSSLifetime"), 0);
exit:
    return;
}

amxd_status_t ra_get_params(amxd_object_t* instance, amxc_var_t* params) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_object_t* ra = amxd_dm_findf(ra_get_dm(), "RouterAdvertisement.");
    global_ra_info_t* global_ra_info = NULL;
    ra_info_t* ra_info = NULL;

    when_null_trace(ra, exit, ERROR, "Failed to get RouterAdvertisement object");
    when_null_trace(params, exit, ERROR, "Output parameters variant is NULL");

    amxc_var_set_type(params, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(instance, params, amxd_dm_access_protected);

    ra_info = (ra_info_t*) instance->priv;
    when_null_trace(ra_info, exit, ERROR, "No ra info available");

    if(!amxc_var_is_null(ra_info->deprecated_prefixes)) {
        amxc_var_t* depr_pref = amxc_var_add_new_key(params, "DeprecatedPrefixes");
        amxc_var_copy(depr_pref, ra_info->deprecated_prefixes);
    }

    global_ra_info = (global_ra_info_t*) ra->priv;
    when_null_trace(global_ra_info, exit, ERROR, "No global ra info available");

    if((!global_ra_info->is_wan_up) || str_empty(GET_CHAR(params, "Prefixes")) || (global_ra_info->is_wan_router_lft_zero) || (ra_info->ipv6_disable_rcvd)) {
        amxc_var_t* lifetime = amxc_var_get_key(params, "AdvDefaultLifetime", AMXC_VAR_FLAG_DEFAULT);
        when_null_trace(lifetime, exit, ERROR, "Failed to get AdvDefaultLifetime");
        amxc_var_set(uint32_t, lifetime, 0);

        SAH_TRACEZ_WARNING(ME, "Setting AdvDefaultLifetime to 0. WAN up: %u, WAN router lifetime 0: %u, IPv6 disabled %u",
                           global_ra_info->is_wan_up, global_ra_info->is_wan_router_lft_zero, ra_info->ipv6_disable_rcvd);

        if(ra_info->ipv6_disable_rcvd) {
            set_all_lifetimes_zero(params);
        }
    }

    // Don't send RAs when no (valid or deprecated) prefixes are available
    if(str_empty(GET_CHAR(params, "Prefixes")) && str_empty(GET_CHAR(params, "ManualPrefixes"))
       && !ra_info->ipv6_disable_rcvd && amxc_var_is_null(ra_info->deprecated_prefixes)) {
        amxc_var_set(bool, GET_ARG(params, "Enable"), false);
    }

    rv = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void ra_update_mod_init(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ra_obj = amxd_dm_findf(ra_get_dm(), "RouterAdvertisement.");
    amxd_object_t* ra_is_obj = amxd_object_findf(ra_obj, "InterfaceSetting.");

    when_null_trace(ra_obj, exit, ERROR, "Couldn't find RouterAdvertisment object");
    when_null_trace(ra_is_obj, exit, ERROR, "Couldn't find RouterAdvertisment.InterfaceSetting object");

    amxd_object_iterate(instance, it, ra_is_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        if(instance->priv == NULL) {
            ra_info_create(instance);
            ra_update_prefix_subscription(instance);
            ra_update(instance, true, NORMAL_UPDATE);
        }
    }
    global_ra_info_create(ra_obj);
    ra_netmodel_subscribe_wan(ra_obj);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void ra_update_mod_cleanup(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ra_obj = amxd_dm_findf(ra_get_dm(), "RouterAdvertisement.");
    amxd_object_t* ra_is_obj = amxd_object_findf(ra_obj, "InterfaceSetting.");

    when_null_trace(ra_obj, exit, ERROR, "Couldn't find RouterAdvertisment object");
    when_null_trace(ra_is_obj, exit, ERROR, "Couldn't find RouterAdvertisment.InterfaceSetting object");

    amxd_object_iterate(instance, it, ra_is_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        if(instance->priv != NULL) {
            ra_info_clear(instance);
        }
    }
    global_ra_info_clear(ra_obj);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
