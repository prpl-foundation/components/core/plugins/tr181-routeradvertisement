/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "ra_object_info.h"
#include <stdlib.h>

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

amxd_status_t ra_info_create(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    ra_info_t* ra_info = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(obj, exit, ERROR, "invalid obj given");
    when_false_trace(obj->priv == NULL, exit, ERROR, "Stop creating a ra info, private data is not NULL");

    ra_info = (ra_info_t*) calloc(1, sizeof(ra_info_t));
    when_null_trace(ra_info, exit, ERROR, "Failed to calloc a ra_info_t");

    obj->priv = ra_info;
    ra_info->obj = obj;
    ra_info->send_mod_data = NULL;
    ra_info->deprecated_prefixes = NULL;
    ra_info->depr_pref_timer = NULL;
    ra_info->nm_query_isUp = NULL;
    ra_info->nm_query_rdnss_address = NULL;

    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t global_ra_info_create(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    global_ra_info_t* global_ra_info = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(obj, exit, ERROR, "RouterAdvertisement object is NULL");
    when_not_null_trace(obj->priv, exit, ERROR, "RouterAdvertisement object private data is not NULL");

    global_ra_info = calloc(1, sizeof(global_ra_info_t));
    when_null_trace(global_ra_info, exit, ERROR, "Failed to allocate private data structure for RouterAdvertisement object");

    obj->priv = global_ra_info;
    global_ra_info->obj = obj;
    global_ra_info->is_wan_up = false;
    global_ra_info->is_wan_router_lft_zero = true;
    global_ra_info->nm_query_wan_is_static = NULL;
    global_ra_info->nm_query_wan_isUp = NULL;
    global_ra_info->nm_query_wan_router_lft = NULL;

    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

void global_ra_info_clear(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    global_ra_info_t* global_ra_info = NULL;

    when_null_trace(obj, exit, ERROR, "RouterAdvertisement object is NULL");
    global_ra_info = (global_ra_info_t*) obj->priv;
    when_null_trace(global_ra_info, exit, ERROR, "RouterAdvertisement object private info is NULL");

    netmodel_closeQuery(global_ra_info->nm_query_wan_isUp);
    netmodel_closeQuery(global_ra_info->nm_query_wan_is_static);
    netmodel_closeQuery(global_ra_info->nm_query_wan_router_lft);
    global_ra_info->nm_query_wan_isUp = NULL;
    global_ra_info->nm_query_wan_is_static = NULL;
    global_ra_info->nm_query_wan_router_lft = NULL;
    global_ra_info->obj = NULL;
    free(global_ra_info);
    obj->priv = NULL;
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void ra_info_clear(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    ra_info_t* ra_info = NULL;

    when_null_trace(obj, exit, ERROR, "no valid object given");
    ra_info = (ra_info_t*) obj->priv;
    when_null_trace(ra_info, exit, ERROR, "object does not have valid ra_info");

    ra_info_clear_content(ra_info);
    ra_info->obj = NULL;
    free(ra_info);
    obj->priv = NULL;
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void ra_info_clear_content(ra_info_t* ra_info) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(ra_info, exit, ERROR, "Object does not have valid ra_info");

    if(ra_info->send_mod_data != NULL) {
        amxc_var_delete(&ra_info->send_mod_data);
        ra_info->send_mod_data = NULL;
    }

    if(ra_info->deprecated_prefixes != NULL) {
        amxc_var_delete(&ra_info->deprecated_prefixes);
        ra_info->deprecated_prefixes = NULL;
    }

    if(ra_info->depr_pref_timer != NULL) {
        amxp_timer_delete(&(ra_info->depr_pref_timer));
        ra_info->depr_pref_timer = NULL;
    }

    if(ra_info->nm_query_isUp != NULL) {
        netmodel_closeQuery(ra_info->nm_query_isUp);
        ra_info->nm_query_isUp = NULL;
    }

    if(ra_info->nm_query_rdnss_address != NULL) {
        netmodel_closeQuery(ra_info->nm_query_rdnss_address);
        ra_info->nm_query_rdnss_address = NULL;
    }

    if(ra_info->nm_query_prefix != NULL) {
        netmodel_closeQuery(ra_info->nm_query_prefix);
        ra_info->nm_query_prefix = NULL;
    }

    ra_info->ipv6_disable_rcvd = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
