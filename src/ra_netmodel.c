/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "ra_update_mod.h"
#include "ra_netmodel.h"

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/
static void trans_set_list_as_csv(amxd_trans_t* trans, amxc_var_t* list, const char* param_name) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t tmp_string;
    amxc_string_init(&tmp_string, 0);

    amxc_string_csv_join_var(&tmp_string, list);
    amxd_trans_set_value(csv_string_t, trans, param_name, amxc_string_is_empty(&tmp_string) ? "" : amxc_string_get(&tmp_string, 0));

    amxc_string_clean(&tmp_string);
    SAH_TRACEZ_OUT(ME);
}

static void ra_netmodel_prefixes_changed_cb(UNUSED const char* const sig_name,
                                            const amxc_var_t* const data,
                                            void* const priv) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ra_info_t* info = (ra_info_t*) priv;
    amxc_string_t tmp_string;
    amxc_string_t manual_prefixes_string;
    amxc_var_t prefixlist_var;
    amxc_var_t autonomouslist_var;
    amxc_var_t on_link_var;
    amxc_var_t actual_prefixlist_var;
    amxc_var_t relative_valid_lifetimes_var;
    amxc_var_t relative_preferred_lifetimes_var;
    amxd_trans_t trans;

    const char* interface_path = NULL;
    const char* manual_prefixes = NULL;

    amxc_string_init(&tmp_string, 0);
    amxc_string_init(&manual_prefixes_string, 0);
    amxd_trans_init(&trans);
    amxc_var_init(&prefixlist_var);
    amxc_var_init(&autonomouslist_var);
    amxc_var_init(&on_link_var);
    amxc_var_init(&actual_prefixlist_var);
    amxc_var_init(&relative_valid_lifetimes_var);
    amxc_var_init(&relative_preferred_lifetimes_var);

    when_null_trace(info, exit, ERROR, "Bad input parameter obj");
    when_null_trace(info->obj, exit, ERROR, "Could not find the param obj in info");

    amxc_var_set_type(&prefixlist_var, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&autonomouslist_var, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&on_link_var, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&actual_prefixlist_var, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&relative_valid_lifetimes_var, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&relative_preferred_lifetimes_var, AMXC_VAR_ID_LIST);

    interface_path = GET_CHAR(amxd_object_get_param_value(info->obj, "Interface"), NULL);
    manual_prefixes = GET_CHAR(amxd_object_get_param_value(info->obj, "ManualPrefixes"), NULL);
    when_str_empty_trace(interface_path, exit, ERROR, "Interface path should not be empty");

    amxc_string_setf(&manual_prefixes_string, "%s", manual_prefixes);

    amxc_var_for_each(netmodel_prefix, data) {
        int index = GET_UINT32(netmodel_prefix, "Index");
        const char* netmodel_static_type = GET_CHAR(netmodel_prefix, "StaticType");
        const char* netmodel_origin_type = GET_CHAR(netmodel_prefix, "Origin");

        if(str_empty(netmodel_static_type) || str_empty(netmodel_origin_type)) {
            SAH_TRACEZ_ERROR(ME, "StaticType is empty, skipping the prefix");
            continue;
        }
        amxc_string_setf(&tmp_string, "%sIPv6Prefix.%d.", interface_path, index);
        if((strcmp(netmodel_static_type, "Static") == 0) && (strcmp(netmodel_origin_type, "Static") == 0)) {
            if(strstr(manual_prefixes, amxc_string_get(&tmp_string, 7)) == NULL) {
                continue;
            }
        }

        if(index > 0) {
            amxc_var_add(cstring_t, &prefixlist_var, amxc_string_get(&tmp_string, 0));
            amxc_var_add(bool, &autonomouslist_var, GET_BOOL(netmodel_prefix, "Autonomous"));
            amxc_var_add(bool, &on_link_var, GET_BOOL(netmodel_prefix, "OnLink"));
            amxc_var_add(cstring_t, &actual_prefixlist_var, GET_CHAR(netmodel_prefix, "Prefix"));
            amxc_var_add(uint32_t, &relative_valid_lifetimes_var, GET_UINT32(netmodel_prefix, "RelativeValidLifetime"));
            amxc_var_add(uint32_t, &relative_preferred_lifetimes_var, GET_UINT32(netmodel_prefix, "RelativePreferredLifetime"));
            if(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &prefixlist_var)) >= 8) {     // 8 being the max amount of prefixes (like in the tr181 documentation)
                break;
            }
        }
    }

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, info->obj);
    trans_set_list_as_csv(&trans, &prefixlist_var, "Prefixes");
    trans_set_list_as_csv(&trans, &autonomouslist_var, "Autonomous");
    trans_set_list_as_csv(&trans, &on_link_var, "OnLink");
    trans_set_list_as_csv(&trans, &actual_prefixlist_var, "ConvertedPrefixes");
    trans_set_list_as_csv(&trans, &relative_valid_lifetimes_var, "RelativeValidLifetimes");
    trans_set_list_as_csv(&trans, &relative_preferred_lifetimes_var, "RelativePreferredLifetimes");
    rv = amxd_trans_apply(&trans, ra_get_dm());
    when_failed_trace(rv, exit, ERROR, "Failed update prefix information with error '%d'", rv);

    ra_update(info->obj, false, FORCE_PLUGIN);

exit:
    amxc_var_clean(&relative_preferred_lifetimes_var);
    amxc_var_clean(&relative_valid_lifetimes_var);
    amxc_var_clean(&actual_prefixlist_var);
    amxc_var_clean(&autonomouslist_var);
    amxc_var_clean(&on_link_var);
    amxc_var_clean(&prefixlist_var);
    amxd_trans_clean(&trans);
    amxc_string_clean(&manual_prefixes_string);
    amxc_string_clean(&tmp_string);
    SAH_TRACEZ_OUT(ME);
}

void ra_netmodel_open_ipv6prefix_query(ra_info_t* ra_info) {
    const char* query_flags = NULL;
    const amxc_var_t* interface = NULL;
    const amxc_var_t* manual_prefixes = NULL;
    amxd_object_t* instance = NULL;

    when_null_trace(ra_info, exit, ERROR, "Priv structure of RouterAdvertisement instance is empty, not opening the NM query");

    instance = ra_info->obj;
    when_null_trace(instance, exit, ERROR, "Failed to find the changed instance");

    interface = amxd_object_get_param_value(instance, "Interface");
    when_null_trace(interface, exit, ERROR, "Could not get the interface parameter");

    manual_prefixes = amxd_object_get_param_value(instance, "ManualPrefixes");
    when_null_trace(manual_prefixes, exit, ERROR, "Interface parameter is empty, not opening the NM query");

    if(ra_info->nm_query_prefix != NULL) {
        netmodel_closeQuery(ra_info->nm_query_prefix);
        ra_info->nm_query_prefix = NULL;
    }
    if(!str_empty(GET_CHAR(manual_prefixes, NULL))) {
        query_flags = "RAPrefix || (Static && !Child && !Inapplicable && !PrefixDelegation)";
    } else {
        query_flags = "RAPrefix";
    }
    when_not_null_trace(ra_info->nm_query_prefix, exit, ERROR, "A prefix query already exists");
    ra_info->nm_query_prefix = netmodel_openQuery_getIPv6Prefixes(GET_CHAR(interface, NULL),
                                                                  "RouterAdvertisement",
                                                                  query_flags,
                                                                  netmodel_traverse_this,
                                                                  ra_netmodel_prefixes_changed_cb,
                                                                  (void*) ra_info);
exit:
    return;
}

static void unsubscribe_ipv6prefix(ra_info_t* info) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    when_null_trace(info, exit, ERROR, "Bad input parameter info");

    if(info->nm_query_prefix != NULL) {
        netmodel_closeQuery(info->nm_query_prefix);
        info->nm_query_prefix = NULL;
    }

    amxd_trans_select_object(&trans, info->obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(csv_string_t, &trans, "Prefixes", "");

    rv = amxd_trans_apply(&trans, ra_get_dm());
    when_failed_trace(rv, exit, ERROR, "Failed to clear Prefixes parameter, return '%d'", rv);

exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return;
}

static void ra_netmodel_interface_isup_cb(UNUSED const char* const sig_name,
                                          const amxc_var_t* const data,
                                          void* const priv) {
    SAH_TRACEZ_IN(ME);
    ra_info_t* info = (ra_info_t*) priv;

    when_null_trace(info, exit, ERROR, "no radvd_info_t found in the private data");
    when_null_trace(info->obj, exit, ERROR, "no instance found in info");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    if(GET_BOOL(data, NULL)) {
        info->ipv6_disable_rcvd = 0;

        ra_netmodel_open_ipv6prefix_query(info);

    } else {
        unsubscribe_ipv6prefix(info);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void ra_netmodel_wan_interface_isup_cb(UNUSED const char* const sig_name,
                                              const amxc_var_t* const data,
                                              void* const priv) {
    SAH_TRACEZ_IN(ME);
    global_ra_info_t* info = (global_ra_info_t*) priv;

    when_null_trace(info, exit, ERROR, "no global ra info found in the private data");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    amxd_object_t* ra_is_obj = amxd_dm_findf(ra_get_dm(), "RouterAdvertisement.InterfaceSetting.");
    when_null_trace(ra_is_obj, exit, ERROR, "Couldn't find RouterAdvertisment.InterfaceSetting object");

    info->is_wan_up = GET_BOOL(data, NULL);
    amxd_object_iterate(instance, it, ra_is_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        ra_update(instance, false, NORMAL_UPDATE);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void ra_netmodel_wan_router_lft_cb(UNUSED const char* const sig_name,
                                          const amxc_var_t* const data,
                                          void* const priv) {
    SAH_TRACEZ_IN(ME);
    global_ra_info_t* info = (global_ra_info_t*) priv;
    uint32_t new_router_lft = GET_UINT32(data, NULL);

    SAH_TRACEZ_INFO(ME, "Received new WAN router lifetime %u", new_router_lft);

    when_null_trace(info, exit, ERROR, "no global ra info found in the private data");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    // If the WAN router lifetime changes from/to 0, we need to update our own router lifetime
    if(info->is_wan_router_lft_zero != (new_router_lft == 0)) {
        amxd_object_t* ra_is_obj = amxd_dm_findf(ra_get_dm(), "RouterAdvertisement.InterfaceSetting.");
        when_null_trace(ra_is_obj, exit, ERROR, "Couldn't find RouterAdvertisment.InterfaceSetting object");

        info->is_wan_router_lft_zero = (new_router_lft == 0);
        amxd_object_iterate(instance, it, ra_is_obj) {
            amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
            ra_update(instance, false, NORMAL_UPDATE);
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void ra_netmodel_wan_prefix_changed_cb(UNUSED const char* const sig_name,
                                              const amxc_var_t* const data,
                                              void* const priv) {
    SAH_TRACEZ_IN(ME);
    global_ra_info_t* info = (global_ra_info_t*) priv;
    char* wan_intf_str = NULL;

    when_null_trace(info, exit, ERROR, "no global ra info found in the private data");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    wan_intf_str = amxd_object_get_value(cstring_t, info->obj, "WANInterface", NULL);
    when_null_trace(wan_intf_str, exit, ERROR, "Could not get the WANInterface parameter");

    amxc_var_for_each(prefix, data) {
        int index = GET_UINT32(prefix, "Index");
        const char* origin = GET_CHAR(prefix, "Origin");
        if((index > 0) && !str_empty(origin) && (strcmp("Static", origin) == 0)) {
            info->is_wan_router_lft_zero = false;
            // close query
            if(info->nm_query_wan_router_lft != NULL) {
                netmodel_closeQuery(info->nm_query_wan_router_lft);
                info->nm_query_wan_router_lft = NULL;
            }
            goto exit;
        }
    }

    when_not_null_trace(info->nm_query_wan_router_lft, exit, ERROR, "Already subscribed to WAN router lifetime");
    info->nm_query_wan_router_lft = netmodel_openQuery_getFirstParameter(wan_intf_str,
                                                                         "RouterAdvertisement",
                                                                         "IPv6Router.RouterLifetime",
                                                                         "ipv6",
                                                                         netmodel_traverse_down,
                                                                         ra_netmodel_wan_router_lft_cb,
                                                                         (void*) info);
    when_null_trace(info->nm_query_wan_router_lft, exit, ERROR, "Could not make a netmodel query, is the interface value correct '%s'", wan_intf_str);

exit:
    free(wan_intf_str);
    SAH_TRACEZ_OUT(ME);
}

amxd_status_t ra_netmodel_subscribe_wan(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_var_t* wan_intf = NULL;
    const char* wan_intf_str = NULL;
    global_ra_info_t* info = NULL;


    when_null_trace(obj, exit, ERROR, "Provided ra object is NULL");

    info = (global_ra_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "RouterAdvertisement object private info is NULL");

    when_not_null_trace(info->nm_query_wan_isUp, exit, ERROR, "Already subscribed to WAN interface");
    when_not_null_trace(info->nm_query_wan_is_static, exit, ERROR, "Already subscribed to WAN interface");

    wan_intf = amxd_object_get_param_value(obj, "WANInterface");
    when_null_trace(wan_intf, exit, ERROR, "Could not get the WANInterface parameter");
    wan_intf_str = amxc_var_constcast(cstring_t, wan_intf);

    info->nm_query_wan_isUp = netmodel_openQuery_isUp(wan_intf_str, "RouterAdvertisement", "ipv6", netmodel_traverse_down, ra_netmodel_wan_interface_isup_cb, (void*) info);
    when_null_trace(info->nm_query_wan_isUp, exit, ERROR, "Could not make a netmodel query, is the interface value correct '%s'", wan_intf_str);

    info->nm_query_wan_is_static = netmodel_openQuery_getIPv6Prefixes(wan_intf_str, "RouterAdvertisement", "Static", netmodel_traverse_down, ra_netmodel_wan_prefix_changed_cb, (void*) info);
    when_null_trace(info->nm_query_wan_is_static, exit, ERROR, "Could not make a netmodel query, is the interface value correct '%s'", wan_intf_str);

    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

void ra_netmodel_unsubscribe_wan(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    global_ra_info_t* info = NULL;
    when_null_trace(obj, exit, ERROR, "RouterAdvertisement object is NULL");
    info = (global_ra_info_t*) obj->priv;
    when_null_trace(info, exit, ERROR, "Global ra info is NULL");

    if(info->nm_query_wan_isUp != NULL) {
        netmodel_closeQuery(info->nm_query_wan_isUp);
        info->nm_query_wan_isUp = NULL;
    }
    if(info->nm_query_wan_router_lft != NULL) {
        netmodel_closeQuery(info->nm_query_wan_router_lft);
        info->nm_query_wan_router_lft = NULL;
    }
    if(info->nm_query_wan_is_static != NULL) {
        netmodel_closeQuery(info->nm_query_wan_is_static);
        info->nm_query_wan_is_static = NULL;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t ra_netmodel_subscribe_prefixes(ra_info_t* info) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_var_t* interface = NULL;
    const char* interface_str = NULL;

    when_null_trace(info, exit, ERROR, "Could not find the priv parameter");
    when_null_trace(info->obj, exit, ERROR, "Could not find the priv parameter");
    when_false_trace(info->nm_query_isUp == NULL, exit, ERROR, "Query is not NULL, should be NULL");

    interface = amxd_object_get_param_value(info->obj, "Interface");
    when_null_trace(interface, exit, ERROR, "Could not get the interface parameter");
    interface_str = amxc_var_constcast(cstring_t, interface);

    info->nm_query_isUp = netmodel_openQuery_isUp(interface_str, "RouterAdvertisement", "ipv6-up", netmodel_traverse_this, ra_netmodel_interface_isup_cb, (void*) info);
    when_null_trace(info->nm_query_isUp, exit, ERROR, "Could not make a netmodel query, is the interface value correct '%s'", interface_str);
    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

void ra_netmodel_unsubscribe_prefixes(ra_info_t* info) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(info, exit, ERROR, "Bad input parameter info");
    when_null_trace(info->send_mod_data, exit, ERROR, "Could not find send_mod_data in the info struct");

    if(info->nm_query_isUp != NULL) {
        netmodel_closeQuery(info->nm_query_isUp);
        info->nm_query_isUp = NULL;
    }

    unsubscribe_ipv6prefix(info);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
