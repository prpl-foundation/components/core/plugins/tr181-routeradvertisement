/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxb/amxb_types.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "test_mock.h"
#include "test_module.h"

static amxd_dm_t dm;
static amxo_parser_t parser;

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout);

int dummy_add_is(const char* func_name,
                 amxc_var_t* args,
                 amxc_var_t* ret);

int __wrap_amxb_call(UNUSED amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     UNUSED int timeout) {
    assert_string_equal(object, "uci.");
    assert_string_equal(GET_CHAR(args, "config"), "dhcp");
    if(strcmp(method, "get") == 0) {
        amxc_var_t values;
        amxc_var_init(&values);
        amxc_var_t ra_flags;
        amxc_var_init(&ra_flags);
        amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        amxc_var_set_type(&ra_flags, AMXC_VAR_ID_LIST);
        amxc_var_t* values_ht = amxc_var_add_key(amxc_htable_t, &values, "values", NULL);
        amxc_var_t* dhcp6pool = amxc_var_add_key(amxc_htable_t, values_ht, "dhcp6pool", NULL);
        amxc_var_t* nodhcp6pool = amxc_var_add_key(amxc_htable_t, values_ht, "nodhcp6pool", NULL);

        amxc_var_add_key(cstring_t, dhcp6pool, "ra", "server");
        amxc_var_add_key(cstring_t, dhcp6pool, "interface", "dummy1");
        amxc_var_add_key(cstring_t, dhcp6pool, "ra_preference", "medium");
        amxc_var_add_key(cstring_t, dhcp6pool, "ra_maxinterval", "600");
        amxc_var_add_key(cstring_t, dhcp6pool, "ra_mininterval", "200");
        amxc_var_add_key(cstring_t, dhcp6pool, "ra_lifetime", "1800");
        amxc_var_add_key(cstring_t, dhcp6pool, "ra_reachabletime", "0");
        amxc_var_add_key(cstring_t, dhcp6pool, "ra_retranstime", "0");
        amxc_var_add_key(cstring_t, dhcp6pool, "ra_hoplimit", "0");
        amxc_var_add_key(cstring_t, dhcp6pool, "ra_mtu", "1500");
        amxc_var_add_key(cstring_t, dhcp6pool, "ndproxy_routing", "1");
        amxc_var_add(cstring_t, &ra_flags, "managed-config");
        amxc_var_add(cstring_t, &ra_flags, "other-config");
        amxc_var_add_key(amxc_llist_t, dhcp6pool, "ra_flags", amxc_var_constcast(amxc_llist_t, &ra_flags));
        amxc_var_add_key(cstring_t, nodhcp6pool, "ra", "disable");
        amxc_var_add_key(cstring_t, nodhcp6pool, "interface", "dummy2");
        amxc_var_add(amxc_htable_t, ret, amxc_var_constcast(amxc_htable_t, &values));
        amxc_var_clean(&values);
        amxc_var_clean(&ra_flags);
    } else if(strcmp(method, "set") == 0) {
        amxc_var_dump(args, STDOUT_FILENO);
    }

    return 0;
}

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxc_var_t data, ret, copy, nm_data;

    amxc_var_init(&data);
    amxc_var_init(&copy);
    amxc_var_init(&ret);
    amxc_var_init(&nm_data);

    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    amxm_module_register(&mod, so, "dm-mngr");
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);

    amxc_var_set(cstring_t, &nm_data, "nm_data");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &data, "Enable", true);
    amxc_var_add_key(cstring_t, &data, "Alias", "dhcp6pool");
    amxc_var_add_key(cstring_t, &data, "Interface", "lan");
    amxc_var_add_key(bool, &data, "AdvManagedFlag", true);
    amxc_var_add_key(bool, &data, "AdvOtherConfigFlag", true);
    amxc_var_add_key(uint32_t, &data, "MaxRtrAdvInterval", 600);
    amxc_var_add_key(uint32_t, &data, "MinRtrAdvInterval", 200);
    amxc_var_add_key(uint32_t, &data, "AdvDefaultLifetime", 1800);
    amxc_var_add_key(bool, &data, "AdvNDProxyFlag", true);
    amxc_var_copy(&copy, &data);

    /* calls to the module api to write are not validated on code, just the amxc_var_dumps */
    assert_int_equal(amxm_execute_function("target_module", "mod-ra-uci", "mod-update", &data, &ret), 0);
    assert_int_equal(amxm_execute_function("target_module", "mod-ra-uci", "mod-remove", &copy, &ret), 0);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    amxc_var_clean(&copy);
    amxc_var_clean(&nm_data);

    handle_events();
}
