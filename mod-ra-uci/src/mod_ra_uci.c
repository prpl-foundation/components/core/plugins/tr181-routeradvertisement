/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ra_uci.h"
#include "uci_ubus.h"
#include "uci_info_list.h"

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

struct _mod_ra_uci {
    amxm_shared_object_t* so;
};

/**********************************************************
* Variable declarations
**********************************************************/

static struct _mod_ra_uci uci;

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

static void uci_alias_changed_cb(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 void* const priv) {
    uci_info_t* info = (uci_info_t*) priv;
    amxd_status_t status = amxd_status_unknown_error;
    char* intf_alias = NULL;

    when_null_trace(info, exit, ERROR, "no uci_info_t found in the private data");
    when_null_trace(data, exit, ERROR, "Invalid data parameter");

    intf_alias = amxc_var_dyncast(cstring_t, data);
    if(str_empty(intf_alias) && str_empty(info->intf_alias)) {
        goto exit;
    }
    if(!str_empty(intf_alias) && !str_empty(info->intf_alias) && (strcmp(info->intf_alias, intf_alias) == 0)) {
        goto exit;
    }

    if(!str_empty(info->intf_alias)) {
        status = uci_ubus_remove_intf(info->intf_alias);
        when_failed_trace(status, exit, ERROR, "failed to remove intf in uci");
        free(info->intf_alias);
        info->intf_alias = NULL;
    }

    if(!str_empty(intf_alias)) {
        info->intf_alias = intf_alias;
        intf_alias = NULL;
        status = uci_ubus_update_intf(info);
        when_failed_trace(status, exit, ERROR, "failed to update intf in uci");
    }
exit:
    free(intf_alias);
    return;

}

static int uci_update_is(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    uci_info_t* info = NULL;
    SAH_TRACEZ_INFO(ME, "got a uci update amxm call");
    const char* intf_path = NULL;

    intf_path = GET_CHAR(args, "Interface");
    when_null_trace(intf_path, exit, ERROR, "Could not find the interface path");

    info = uci_info_list_get(intf_path);
    if(info == NULL) {
        status = uci_info_add(args, uci_alias_changed_cb);
        when_failed_trace(status, exit, ERROR, "failed to add info to the list");
    } else {
        status = uci_info_update(args, info);
        when_failed_trace(status, exit, ERROR, "failed to update info to the list");

        status = uci_ubus_update_intf(info);
        when_failed_trace(status, exit, ERROR, "failed to update intf in uci");
    }
exit:
    return (int) status;
}

static int uci_remove_is(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    const char* intf_path = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    uci_info_t* info = NULL;

    SAH_TRACEZ_INFO(ME, "got a uci remove amxm call");

    intf_path = GET_CHAR(args, "Interface");
    when_null_trace(intf_path, exit, ERROR, "Could not find the interface path");

    info = uci_info_list_get(intf_path);
    when_null_trace(info, exit, ERROR, "Could not find an interface %s", intf_path);
    when_str_empty_status(info->intf_alias, exit, status = amxd_status_ok);

    status = uci_ubus_remove_intf(info->intf_alias);
    when_failed_trace(status, exit, ERROR, "failed to remove intf in uci");

    status = uci_info_list_delete(intf_path);
    when_failed_trace(status, exit, ERROR, "Failed to delete intf_path %s from the list", intf_path);
exit:
    return (int) status;
}

static int uci_shutdown(UNUSED const char* function_name,
                        UNUSED amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_WARNING(ME, "The shutdown function is not implemented for this module");
    return 0;
}

static AMXM_CONSTRUCTOR ra_uci_start(void) {
    amxm_module_t* mod = NULL;

    uci_info_list_init();

    uci.so = amxm_so_get_current();
    amxm_module_register(&mod, uci.so, MOD_RA_UCI);
    amxm_module_add_function(mod, "mod-update", uci_update_is);
    amxm_module_add_function(mod, "mod-remove", uci_remove_is);
    amxm_module_add_function(mod, "mod-shutdown", uci_shutdown);

    return 0;
}

static AMXM_DESTRUCTOR ra_uci_stop(void) {

    uci_info_list_clean();
    amxm_so_close(&uci.so);
    uci.so = NULL;
    return 0;
}
