/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <string.h>
#include <stdlib.h>

#include "uci_ubus.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

/**********************************************************
* Macro definitions
**********************************************************/

#define str_empty(x) ((x == NULL) || (*x == 0))

/**********************************************************
* Type definitions
**********************************************************/

enum RA_UCI_PARAMS {
    RA_MAXINTERVAL,
    RA_MININTERVAL,
    RA_LIFETIME,
    RA_MTU,
    RA_REACHABLETIME,
    RA_RETRANSTIME,
    RA_HOPLIMIT,
    RA_END
};

/**********************************************************
* Variable declarations
**********************************************************/

struct ra_param_translation {
    const char* uci;
    const char* tr181;
} tr181_translation[] = {
    { "ra_maxinterval", "MaxRtrAdvInterval" },
    { "ra_mininterval", "MinRtrAdvInterval" },
    { "ra_lifetime", "AdvDefaultLifetime" },
    { "ra_mtu", "AdvLinkMTU" },
    { "ra_reachabletime", "AdvReachableTime" },
    { "ra_retranstime", "AdvRetransTimer" },
    { "ra_hoplimit", "AdvCurHopLimit" },
};

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

static char* handle_alias_parameter(const char* alias) {
    char* ptr = NULL;
    char* tr181_alias = NULL;

    when_str_empty_trace(alias, exit, ERROR, "Invalid orig input param");

    if(strstr(alias, "-") != NULL) {
        tr181_alias = strdup(alias);

        ptr = strstr(tr181_alias, "-");
        if(ptr != NULL) {
            *ptr = '_';
            while((ptr = strstr(ptr, "-"))) {
                *ptr = '_';
            }
        }
    }
exit:
    return tr181_alias;
}

static amxd_status_t translate_tr181_to_uci(uci_info_t* info, amxc_var_t* dest) {
    int i = -1;
    amxc_var_t* ra_flags = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    char* tr181_alias = NULL;

    when_null_trace(info, exit, ERROR, "Invalid info input param");
    when_null_trace(info->args, exit, ERROR, "Invalid info args input param");
    when_null_trace(dest, exit, ERROR, "Invalid dest input param");
    when_str_empty_trace(info->intf_alias, exit, ERROR, "Invalid alias input param");

    for(i = 0; i < RA_END; i++) {
        amxc_var_t* tmp = GET_ARG(info->args, tr181_translation[i].tr181);
        if(tmp != NULL) {
            amxc_var_add_key(uint32_t, dest, tr181_translation[i].uci, GET_UINT32(info->args, tr181_translation[i].tr181));
        }
    }

    if(GET_ARG(info->args, "Enable") != NULL) {
        bool enable = GET_BOOL(info->args, "Enable");
        if(enable) {
            amxc_var_add_key(cstring_t, dest, "ra", "server");
            amxc_var_add_key(cstring_t, dest, "ignore", "0");
        } else {
            amxc_var_add_key(cstring_t, dest, "ra", "disabled");
        }
    }

    tr181_alias = handle_alias_parameter(info->intf_alias);
    if(tr181_alias != NULL) {
        amxc_var_add_key(cstring_t, dest, "ra181alias", tr181_alias);
        free(tr181_alias);
    }

    if(GET_ARG(info->args, "AdvPreferredRouterFlag") != NULL) {
        const char* flag = GET_CHAR(info->args, "AdvPreferredRouterFlag");
        const char* str = NULL;
        if(flag != NULL) {
            if(strcmp(flag, "Low") == 0) {
                str = "low";
            } else if(strcmp(flag, "Medium") == 0) {
                str = "medium";
            } else if(strcmp(flag, "High") == 0) {
                str = "high";
            }
            amxc_var_add_key(cstring_t, dest, "ra_preference", str);
        }
    }

    ra_flags = amxc_var_add_key(amxc_llist_t, dest, "ra_flags", NULL);
    if((GET_ARG(info->args, "AdvManagedFlag") != NULL) &&
       (GET_BOOL(info->args, "AdvManagedFlag") == true)) {
        amxc_var_add(cstring_t, ra_flags, "managed-config");
    }
    if((GET_ARG(info->args, "AdvOtherConfigFlag") != NULL) &&
       (GET_BOOL(info->args, "AdvOtherConfigFlag") == true)) {
        amxc_var_add(cstring_t, ra_flags, "other-config");
    }
    if((GET_ARG(info->args, "AdvMobileAgentFlag") != NULL) &&
       (GET_BOOL(info->args, "AdvMobileAgentFlag") == true)) {
        amxc_var_add(cstring_t, ra_flags, "home-agent");
    }
    if(amxc_llist_size(amxc_var_constcast(amxc_llist_t, ra_flags)) == 0) {
        amxc_var_add(cstring_t, ra_flags, "none");
    }

    if(GET_ARG(info->args, "AdvNDProxyFlag") != NULL) {
        amxc_var_add_key(bool, dest, "ndproxy_routing", GET_BOOL(info->args, "AdvNDProxyFlag"));
    }
    status = amxd_status_ok;
exit:
    return status;
}

static int uci_call(const char* method,
                    const char* section,
                    const char* type,
                    amxc_var_t* values,
                    amxc_var_t* result) {
    amxd_status_t status = amxd_status_unknown_error;
    int ret = -1;
    amxc_var_t args;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("uci.");
    char* tr181_alias = NULL;
    amxc_var_t* tmp = NULL;

    amxc_var_init(&args);

    when_null_trace(ctx, exit, ERROR, "Failed to get the ctx for uci");
    when_str_empty_trace(method, exit, ERROR, "Invalid input param method");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "config", "dhcp");

    if(section) {
        bool add = (strncmp(method, "add", 3) == 0);
        tr181_alias = handle_alias_parameter(section);
        amxc_var_add_key(cstring_t, &args, (add ? "name" : "section"), (tr181_alias != NULL ? tr181_alias : section));
        free(tr181_alias);
    }

    if(type) {
        amxc_var_add_key(cstring_t, &args, "type", type);
    }

    if(values) {
        tmp = amxc_var_add_key(amxc_htable_t, &args, "values", NULL);
        amxc_var_move(tmp, values);
    }
    ret = amxb_call(ctx, "uci.", method, &args, result, 3);
    when_false_trace(ret == 0, exit, ERROR, "Failed to excute the uci call");
    status = amxd_status_ok;
exit:
    amxc_var_clean(&args);
    return status;
}

amxd_status_t uci_ubus_update_intf(uci_info_t* info) {
    int retval = 0;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t uci_args;
    amxc_var_t result;
    bool found_result = false;

    amxc_var_init(&result);
    amxc_var_init(&uci_args);

    when_null_trace(info, exit, ERROR, "Invalid input param info");
    when_true_status(str_empty(info->intf_alias), exit, status = amxd_status_ok);

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    status = translate_tr181_to_uci(info, &uci_args);
    when_failed_trace(status, exit, ERROR, "Failed to convert tr181 var to uci var, for %s", info->intf_alias);

    status = uci_call("get", info->intf_alias, "dhcp", NULL, &result);
    when_failed_trace(status, exit, ERROR, "Failed to get the interface in uci, for %s", info->intf_alias);
    found_result = (amxc_var_get_first(&result));
    amxc_var_clean(&result);
    status = uci_call((found_result ? "set" : "add"), info->intf_alias, "dhcp", &uci_args, &result);
    when_failed_trace(status, exit, ERROR, "Failed to update the interface in uci, for %s", info->intf_alias);
    amxc_var_clean(&result);
    status = uci_call("commit", NULL, NULL, NULL, &result);
    when_failed_trace(status, exit, ERROR, "Failed to commit the changes in uci, for %s", info->intf_alias);
exit:
    amxc_var_clean(&result);
    amxc_var_clean(&uci_args);
    return retval;
}

amxd_status_t uci_ubus_remove_intf(const char* intf_alias) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t uci_args;
    amxc_var_t result;

    amxc_var_init(&uci_args);
    amxc_var_init(&result);

    when_str_empty_trace(intf_alias, exit, ERROR, "Invalid input param intf_alias");

    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &uci_args, "ra", "disabled");

    status = uci_call("set", intf_alias, "dhcp", &uci_args, &result);
    when_failed_trace(status, exit, ERROR, "Failed to update the interface in uci, for %s", intf_alias);
    amxc_var_clean(&result);

    status = uci_call("commit", NULL, NULL, NULL, &result);
    when_failed_trace(status, exit, ERROR, "Failed to commit the changes in uci, for %s", intf_alias);
exit:
    amxc_var_clean(&result);
    amxc_var_clean(&uci_args);
    return status;
}